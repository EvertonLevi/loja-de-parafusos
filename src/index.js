import React, { Component } from 'react';
import ReactDOM, { render } from 'react-dom';
import './index.css';

import { Provider } from 'react-redux'
import { store } from '../src/store/helpers'
import { Rotas } from './Rotas';
// import * as serviceWorker from './serviceWorker';

import { configureFakeBackend } from '../src/store/helpers'
configureFakeBackend();

render(
 <Provider store={store} >
  <Rotas alert={{ type: "erro", message: "deu erro" }} />
 </Provider>
 , document.getElementById('root')
)
























// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA

// serviceWorker.unregister();
